# Public Kubernetes Cluster

This project is the configuration of the Public Nexylan Kubernetes Cluster used to deploy services to the world.

## Limitations

Due to the lack of some features from Gitlab GitOPS management system.

We are currently obliged to deploy manifests and inject into this repository `vendor code`.

HELM charts are not supported at this state.

GitOPS from Gitlab is more designed for self-made Kubernetes Deployment files.
